
"""
create_index(index::String)

Create a new ElasticSearch index with name given in parameter.

Returns the response body from ElasticSearch server (Dict).

If an error occurs, it returns the error body (Dict).
"""
function create_index(index::String)
response = Dict{String,Any}()
if index != ""
    try
        r = HTTP.request("PUT", base_url * index, [], "")
        response = JSON.parse(String(r.body))
    catch e
        response = JSON.parse(String(e.response.body))
    end
end
return response
end # create_index


"""
create_index(index::String, mapping)

Create a new ElasticSearch index with name and mapping given in parameter.

Returns the response body from ElasticSearch server (Dict).

If an error occurs, it returns the error body (Dict).
"""
function create_index(index::String, mapping::Dict)
response = Dict{String,Any}()
if index != ""
    try
        r = HTTP.request("PUT", base_url * index,
                        ["Content-Type" => "application/json"],
                        JSON.json(mapping))
        response = JSON.parse(String(r.body))
    catch e
        response = JSON.parse(String(e.response.body))
    end
end
return response
end # create_index with mapping


"""
delete_index(index::String)

Delete an ElasticSearch index given its name

Returns the response body from ElasticSearch server (Dict).

If an error occurs, it returns the error body (Dict).
"""
function delete_index(index::String)
response = Dict{String,Any}()
if index != ""
    try
        r = HTTP.request("DELETE", base_url * index,[],"")
        response = JSON.parse(String(r.body))
    catch e
        response = JSON.parse(String(e.response.body))
    end
end
return response
end # delete_index

"""
get_index(index::String)

Get an ElasticSearch index given its name

Returns the response body from ElasticSearch server (Dict).

If an error occurs, it returns the error body (Dict).
"""
function get_index(index::String)
response = Dict{String,Any}()
try
    r = HTTP.request("GET", base_url * index,[],"")
    response = JSON.parse(String(r.body))
catch e
    response = JSON.parse(String(e.response.body))
end
return response
end # get_index
