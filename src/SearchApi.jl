"""
    search_term(term::String, search::String) doc::Dict

Get document of the given type, with the given id, from the given index

Returns the response body from ElasticSearch server (Dict).

If an error occurs, it returns the error body (Dict).
"""
function search_term(term::String, search::String)
    doc = Dict("query" => Dict( "term" => Dict( term => search ) ) )
    response = Dict{String,Any}()
    if term != ""
        try
            r = HTTP.request("GET", base_url * index * "/_search",
                            ["Content-Type" => "application/json"],
                            JSON.json(doc))
            response = JSON.parse(String(r.body))
        catch e
            response = JSON.parse(String(e.response.body))
        end
    end
    return response
end