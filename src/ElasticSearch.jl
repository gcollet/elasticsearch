module ElasticSearch
include("DocumentApi.jl")
include("IndexApi.jl")
include("SearchApi.jl")
using HTTP
using JSON

# global variable to store the URl of the server
base_url = ""

"""
    set_base_url(url::String)

Set the base URL to access the server [Default=""]

Returns true
"""
function set_base_url(url::String)
    global base_url = url
    return true
end

"""
    get_base_url()

Returns the base URL to access the server
"""
function get_base_url()
    return base_url
end

end # module ElasticSearch
