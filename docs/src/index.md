# ElasticSearch.jl Documentation

```@meta
CurrentModule = ElasticSearch
```

# Example.jl Documentation

```@contents
```

## Functions


```@docs
create_index(index::String)
```
```@docs
create_index(index::String, mapping)
```

## Index

```@index
```
