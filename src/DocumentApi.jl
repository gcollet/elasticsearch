"""
add_document(index::String, type::String, id::String, doc::Dict)

Adds document doc of the given type, with the given id, to the given index

Returns the response body from ElasticSearch server (Dict).

If an error occurs, it returns the error body (Dict).
"""
function add_document(index::String, type::String, id::String, doc::Dict)
    response = Dict{String,Any}()
    if index != ""
        try
            r = HTTP.request("PUT", base_url * index * "/" * type * "/" * id,
                            ["Content-Type" => "application/json"],
                            JSON.json(doc))
            response = JSON.parse(String(r.body))
        catch e
            response = JSON.parse(String(e.response.body))
        end
    end
    return response
end

"""
get_document(index::String, type::String, id::String) doc::Dict

Get document of the given type, with the given id, from the given index

Returns the response body from ElasticSearch server (Dict).

If an error occurs, it returns the error body (Dict).
"""
function get_document(index::String, type::String, id::String)
    response = Dict{String,Any}()
    if index != ""
        try
            r = HTTP.request("GET", base_url * index * "/" * type * "/" * id)
            response = JSON.parse(String(r.body))
        catch e
            response = JSON.parse(String(e.response.body))
        end
    end
    return response
end
