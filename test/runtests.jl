using ElasticSearch
using Test
using JSON
using CSV
using DataFrames


# Mapping is a dictionary to test create_index
map_str = "{\"mappings\":{\"person\":{\"properties\":{\"title\":{\"type\":\"text\"},\"name\":{\"type\":\"text\"},\"age\":{\"type\":\"integer\"},\"created\":{\"type\":\"date\",\"format\":\"strict_date_optional_time||epoch_millis\"}}}}}\n"
mapping = JSON.parse(map_str)
host = "http://localhost:9200/"

@testset "Base URL methods" begin
    res = ElasticSearch.get_base_url()
    @test isempty(res)
    res = ElasticSearch.set_base_url("toto")
    @test res
    res = ElasticSearch.get_base_url()
    @test res == "toto"
    res = ElasticSearch.set_base_url(host)
    @test res
    res = ElasticSearch.get_base_url()
    @test res == host
end


@testset "Index methods" begin

    ElasticSearch.set_base_url(host)

    # Create empty index without mapping
    res = ElasticSearch.create_index("")
    @test isempty(res)

    # Create empty index with mapping
    res = ElasticSearch.create_index("", mapping)
    @test isempty(res)

    # Delete empty index
    res = ElasticSearch.delete_index("")
    @test isempty(res)

    # Create test index without mapping
    res = ElasticSearch.create_index("test_index_julia")
    @test res["index"] == "test_index_julia"

    # Create test index without mapping -> already created
    res = ElasticSearch.create_index("test_index_julia")
    @test res["error"]["root_cause"][1]["type"] == "resource_already_exists_exception"

    # Delete test index
    res = ElasticSearch.delete_index("test_index_julia")
    @test res["acknowledged"]

    # Delete test index already deleted
    res = ElasticSearch.delete_index("test_index_julia")
    @test res["error"]["root_cause"][1]["reason"] == "no such index"

    # Create test index with mapping
    res = ElasticSearch.create_index("test_index_julia", mapping)
    @test res["index"] == "test_index_julia"

    # Create test index with mapping -> already created
    res = ElasticSearch.create_index("test_index_julia", mapping)
    @test res["error"]["root_cause"][1]["type"] == "resource_already_exists_exception"

    # Clean test environment
    res = ElasticSearch.delete_index("test_index_julia")
end


@testset "Document PUT method" begin
    ElasticSearch.set_base_url(host)

    # Test without mapping
    ElasticSearch.create_index("test_index_julia")
    document = Dict("title" => "M", "name" => "Jolafrite", "age" => 32)
    res = ElasticSearch.add_document("test_index_julia", "person", "jodfl", document)
    @test res["_index"] == "test_index_julia"
    @test res["_type"] == "person"
    @test res["_id"] == "jodfl"
    @test res["_shards"]["successful"] == 1
    ElasticSearch.delete_index("test_index_julia")

    # Test with mapping
    ElasticSearch.create_index("test_index_julia", mapping)
    res = ElasticSearch.add_document("test_index_julia", "person", "jodfl", document)
    @test res["_index"] == "test_index_julia"
    @test res["_type"] == "person"
    @test res["_id"] == "jodfl"
    @test res["_shards"]["successful"] == 1
    bad_doc = Dict("title" => "M", "name" => "Jolafrite", "age" => "kjdnf")
    res = ElasticSearch.add_document("test_index_julia", "person", "jodfl2", bad_doc)
    @test res["error"]["root_cause"][1]["reason"] == "failed to parse field [age] of type [integer]"
    ElasticSearch.delete_index("test_index_julia")
end

@testset "Document GET method" begin
    ElasticSearch.set_base_url(host)

    # Test without mapping
    ElasticSearch.create_index("test_index_julia")
    document = Dict("title" => "M", "name" => "Jolafrite", "age" => 32)
    ElasticSearch.add_document("test_index_julia", "person", "jodfl", document)
    answer = ElasticSearch.get_document("test_index_julia", "person", "jodfl")
    @test answer["_index"] == "test_index_julia"
    @test answer["_type"] == "person"
    @test answer["_id"] == "jodfl"
    @test answer["_source"]["age"] == 32
    ElasticSearch.delete_index("test_index_julia")
end
